const plugin = require('tailwindcss/plugin');
const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
  theme: {
    extend: {
      container: {
        center: true,
        //padding: '1.25rem'
      },
      spacing: {
        'sm': '8px',
        'md': '12px',
        'lg': '16px',
        'xl': '24px',
      },
      fontFamily: {
        'body': [
            '"source_serif_pro"',
            ...defaultTheme.fontFamily.serif
        ],
        'head': [
          '"libre_franklin"',
            ...defaultTheme.fontFamily.sans
        ]
      },
      fontSize: {
        'h6': '1rem',
        'h5': '1.25rem',
        'h4': '1.375rem',
        'h3': '1.625rem',
        'h2': '1.875rem',
        'h1': '2.5rem',
        'hero': '4rem',
        'normal': '1.25rem',
        'category': '0.75rem',
        'small': '0.625',
      },
      lineHeight: {
        'h6': '1.5rem',
        'h5': '1.625rem',
        'h4': '1.75rem',
        'h3': '2rem',
        'h2': '2.5rem',
        'h1': '3rem',
        'hero': '4rem',
        'normal': '2rem',
        'category': '1.25rem',
        'small': '1.25rem',
      },
      spacing: {
        '4-1/2': '1.125rem',
        '5-1/2': '1.375rem',
        '7': '1.75rem',
        '7-1/2': '1.1875rem',
        '9': '2.25rem',
        '11': '2.75rem',
        '12-1/2': '3.125rem',
        '13': '3.25rem',
        '13-1/4': '3.3125rem',
        '13-1/2': '3.375rem',
        '14': '3.5rem',
        '15': '3.75rem',
        '18': '4.5rem',
        '19-1/4': '4.8125rem',
        '30': '7.5rem',
        '72': '18rem',
        '470': '29.375rem'
      },
      colors: {
        other: {
          'dark': '#191919',
          'red': '#BC3428',
          'gray-75': 'rgba(25,25,25, 0.75)',
          'gray-8': 'rgba(25,25,25, 0.08)',
        },
      },
    }
  },
  variants: {
    margin: [ 'responsive', 'first', 'last' ],
    padding: [ 'responsive', 'first', 'last' ],
    opacity: [ 'responsive', 'group-hover', 'hover' ],
    visibility: [ 'responsive', 'group-hover', 'hover' ],
    textDecoration: [ 'responsive', 'group-hover', 'hover' ],
    textColor: [ 'responsive', 'group-hover', 'hover', 'focus' ],
    borderColor: [ 'responsive', 'hover', 'focus', 'group-hover', 'focus' ],
    borderWidth: [ 'responsive', 'first', 'last' ],
    rotate: [ 'responsive', 'hover', 'focus', 'group-hover' ],
    translate: [ 'responsive', 'hover', 'focus', 'group-hover' ],
    scale: [ 'responsive', 'hover', 'focus', 'group-hover' ],
    backfaceVisibility: [ 'responsive', 'hover', 'focus', 'group-hover' ]
  },
  plugins: [
    require('@tailwindcss/ui'),
    require('@tailwindcss/typography'),
    plugin( function ({ addUtilities, variants }) {
      const backfaceVisibility = {
        '.backface-visible': {
          backfaceVisibility: 'visible'
        },
        '.backface-invisible': {
          backfaceVisibility: 'hidden'
        }
      };
      addUtilities( backfaceVisibility, variants('backfaceVisibility') )
    }),
  ],
  purge: {
    mode: 'all',
    content: [
      './**/*.php',
      './assets/**/*.js'
    ],
    options: {
      whitelist: []
    }
  },
  future: {
    removeDeprecatedGapUtilities: true,
  }
};
