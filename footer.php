<?php
    $custom_logo_id = get_theme_mod( 'custom_logo' );
    $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
    $tiktok = get_field('tiktok', 'option');
    $linkedin = get_field('linkedin', 'option');
    $twitter = get_field('twitter', 'option');
    $newsletter_heading = get_field('newsletter_heading', 'option');
    $newsletter_subheading = get_field('newsletter_subheading', 'option');
    $copyrights = get_field('theme_options_copyrights', 'option');
    $footer_bio = get_field('footer_bio', 'option');
?>
<?php echo ( is_page_template( 'templates/template-tiktok.php' ) || is_front_page() || is_404() || is_page_template('templates/template-search.php') || is_page_template('templates/template-newsletter.php') ? '<div class="hidden">' : ''); ?>
<div class="container">
    <div id="newsletter-bottom" class="newsletter newsletter--bottom pb-14 pt-8 bg-other-gray-8 text-center mb-10">
        <div class="container">
            <div class="w-full lg:w-8/12 mx-auto px-4">
                <?php if ( $newsletter_heading ): ?>
                    <div class="text-h2 leading-h2 font-head font-bold mb-4 text-other-dark"><?php echo esc_html( $newsletter_heading ); ?></div>
                <?php endif; ?>
                <?php if ( $newsletter_subheading ): ?>
                    <div class="mb-6 text-normal font-body leading-normal text-other-dark"><?php echo esc_html( $newsletter_subheading ); ?></div>
                <?php endif; ?>
                <?php get_template_part('template-parts/content', 'newsletter-form'); ?>
            </div>
        </div>
    </div>
</div>
<?php echo ( is_page_template('templates/template-search.php') || is_front_page() || is_404() || is_page_template('templates/template-newsletter.php') ? '</div>' : ''); ?>
<footer id="site-footer">
    <div class="container">
        <?php if ( !is_front_page() ): ?>
            <div class="flex flex-wrap w-100 border-t-4 border-other-dark py-4">
                <div class="w-full lg:w-2/12">
                    <?php 
                        if ( has_custom_logo() ) {
                            echo '<a href="' . get_option("siteurl") . '"><img class="lg:mx-0 mx-auto" src="' . esc_url( $logo[0] ) . '" alt="' . get_bloginfo( 'name' ) . '"></a>';
                        } else {
                                echo '<h1><a href="' . get_option("siteurl") . '">'. get_bloginfo( 'name' ) .'</a></h1>';
                        }
                    ?>
                </div>
                <div class="w-full lg:w-8/12">
                    <?php
                        wp_nav_menu( [
                                'menu'            => 'footer',
                                'menu_class'      => 'block lg:flex uppercase font-head justify-between items-center m-auto',
                                'container'       => 'nav',
                                'container_class' => 'menu menu--footer h-full lg:flex text-center',
                                'theme_location'  => 'footer'
                        ] );
                    ?>
                </div>
            <?php else: ?>
                <div class="flex flex-wrap w-100 items-center pt-8 pb-4">
                    <div class="w-full lg:w-5/12">
                        <?php 
                            if ( has_custom_logo() ) {
                                echo '<a class="font-bold" href="' . get_option("siteurl") . '"><img class="lg:mx-0 mx-auto inline" src="' . esc_url( $logo[0] ) . '" alt="' . get_bloginfo( 'name' ) . '"> <span class="inline">brendangahan.com</span></a>';
                            } else {
                                    echo '<h1><a href="' . get_option("siteurl") . '">'. get_bloginfo( 'name' ) .'</a></h1>';
                            }
                        ?>
                    </div>
                    <div class="w-full lg:w-7/12">
                        <div class="footer-bio">
                            <?php 
                                if ($footer_bio){
                                    echo wp_kses( $footer_bio, [ 'p' => [] ] );
                                }
                            ?>
                        </div>  
                    </div>
                    <div class="w-full">
                        <?php
                            wp_nav_menu( [
                                    'menu'            => 'footer_home',
                                    'menu_class'      => 'block lg:flex uppercase font-head justify-between items-right lg:ml-auto',
                                    'container'       => 'nav',
                                    'container_class' => 'menu menu--footer-home h-full lg:flex text-right',
                                    'theme_location'  => 'footer_home'
                            ] );
                        ?>
                    </div>
            <?php endif; ?>
            <?php if ( !is_front_page() ): ?>
                <div class="w-full lg:w-2/12 block lg:flex items-center">
                    <ul class="social-media social-media--footer mx-auto lg:ml-auto lg:mr-0 text-center h-8">
                        <?php if ( $twitter ): ?>
                            <li class="inline-block"><a href="<?php echo $twitter; ?>" title="Twitter" target="_blank"><img width="32" height="26" src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/social/twitter.svg"></a></li>
                        <?php endif; ?>
                        <?php if ( $linkedin ): ?>
                            <li class="inline-block"><a href="<?php echo $linkedin; ?>" title="Linkedin" target="_blank"><img width="27" height="27" src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/social/linkedin-in.svg"></a></li>
                        <?php endif; ?>
                        <?php if ( $tiktok ): ?>
                            <li class="inline-block"><a href="<?php echo $tiktok; ?>" title="Tiktok" target="_blank"><img width="25" height="25" src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/social/tiktok.svg"></a></li>
                        <?php endif; ?>
                    </ul>
                </div>
            <?php endif; ?>
        </div>
        <?php if ( $copyrights && !is_front_page() ): ?>
            <div class="flex py-4 text-center copyrights">
                <p class="block w-full text-category font-head text-other-dark font-medium text-other-dark"><?php echo esc_html( $copyrights ); ?></p>
            </div>
        <?php endif; ?>
    </div>
</footer>
<?php echo ( is_page_template( 'templates/template-tiktok.php' ) ? '</div>' : ''); ?>
<?php wp_footer(); ?>
</body>
</html>
