<?php get_header(); ?>
<?php
    $headline = get_field('fp_heading');
    $subheadline = get_field('fp_subheading');
    $bg = get_field('fp_background_image');
    $above_form = get_field('above_newsletter_form_text');
    $below_form = get_field('below_newsletter_form_text');
    $featured_on = get_field('featured_on');
?>
<section class="hero hero--home h-screen flex bg-other-dark text-white bg-no-repeat relative">
    <?php if ( $bg ): ?>
        <div class="bg-wrap absolute w-1/2 right-0 inset-y-0" style="background-image: url(<?php echo  $bg ?>);">
        </div>
    <?php endif; ?>
    <div class="container mh-600 h-auto m-auto flex content-center flex-wrap">
        <div class="w-full lg:w-8/12">
            <?php if ( $headline ): ?>
                <h1 class="hero-title text-white text-h2 font-head font-bold leading-h2 uppercase mb-8"><?php echo wp_kses( $headline, [ 'br' => [] ] ); ?></h1>
            <?php endif; ?>
            <?php if ( $subheadline ): ?>
                <div class="font-head text-normal leadering-normal mb-10 fp-subtitle"><?php echo wp_kses( $subheadline, [ 'ul' => [], 'li' => [] ] ); ?></div>
            <?php endif; ?>    
            <div id="front-page-newsletter" class="mb-8">
                <?php if ( $above_form ): ?>
                    <p class="font-head text-normal leadering-normal mb-4"><?php esc_html_e($above_form); ?></p>
                <?php endif; ?>
                
                <?php get_template_part('template-parts/content', 'newsletter-form'); ?>
                <?php if ( $below_form ): ?>
                    <p class="font-head text-normal leadering-normal mt-4"><?php esc_html_e($below_form); ?></p>
                <?php endif; ?>
            </div>
            <div class="fp-bottom relative left-0 bottom-0 mb-6 lg:mb-24">
                <a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>" class="font-head uppercase text-white text-h5 leading-h3 font-bold border-other-red border-b-solid border-b-2">Read the blog <svg width="7" height="12" xmlns="http://www.w3.org/2000/svg" class="inline ml-2 mb-1"><path d="M7 6L1.167 0 0 1.2 4.667 6 0 10.8 1.167 12z" fill="#FFF" fill-rule="nonzero"/></svg></a>
            </div> 
        </div>
           
    </div>
</section>
<section class="section section-featured">
    <div class="container container--featured">
        <div class="w-full mx-auto">
            <div class="font-head font-bold"><?php _e('Featured on:', 'simplicity'); ?></div>
            <div class="flex flex-wrap items-center">

                <?php
                    if( have_rows('featured_on') ):

                        // Loop through rows.
                        while( have_rows('featured_on') ) : the_row();

                            // Load sub field value.
                            $logo = get_sub_field('logo');
                            $url = get_sub_field('url');

                            echo '<div class="lg:w-1/5 sm:w-6/12 p-4 text-center">';

                                if ( $url ){
                                    echo '<a href="' . $url . '" target="_blank">';
                                        echo '<img src="' . $logo . '">';
                                    echo '</a>';
                                } else {
                                    echo '<img src="' . $logo . '">';
                                }
                                
                            echo '</div>';
                            // Do something...

                        // End loop.
                        endwhile;

                    endif;
                ?>
            </div>
        </div>
    </div>
</section>
<?php
get_footer();