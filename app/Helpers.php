<?php

namespace Simplicity\Theme;

class Helpers
{

    /**
     * If WP_DEBUG is turned off, add .min prefix to a certain style or script.
     *
     * @return string
     */
    public static function getMinifiedPrefix()
    {

        return WP_DEBUG ? '' : '.min';

    }

}