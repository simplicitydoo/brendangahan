<?php
/**
 * Define our Main Class.
 */

namespace Simplicity\Theme;

class Main
{

    /**
     * Hook a function to "after_setup_theme" action.
     *
     * @param $function Callback function.
     */
    public function afterThemeSetup( $function )
    {

        add_action( 'after_setup_theme', $function );

    }

    /**
     * Hook a function to "wp_enqueue_scripts" action.
     *
     * @param $function Callback function.
     */
    public function enqueueAssets( $function )
    {

        add_action( 'wp_enqueue_scripts', $function );

    }

    /**
     * Hook a function inside a class to "wp_ajax_" and "wp_ajax_nopriv_" actions.
     * @param $function_name  Name of the callback function inside the class.
     * @return $this
     */
    public function wpAjax( $function_name )
    {

        add_action( 'wp_ajax_' . $function_name, [ 'Simplicity\Theme\AJAX', $function_name ] );
        add_action( 'wp_ajax_nopriv_' . $function_name, [ 'Simplicity\Theme\AJAX', $function_name ] );

        return $this;

    }

    /**
     * Hook a function to a filter.
     *
     * @param $tag
     * @param $function_to_add
     * @param  int  $priority
     * @param  int  $accepted_args
     *
     * @return $this
     */
    public function addFilter( $tag, $function_to_add, $priority = 10, $accepted_args = 1 )
    {

        add_filter( $tag, [ 'Simplicity\Theme\Filters', $function_to_add ], $priority, $accepted_args );

        return $this;

    }

    /**
     * Hook a function to an action.
     *
     * @param $tag
     * @param $function_to_add
     * @param  int  $priority
     * @param  int  $accepted_args
     *
     * @return $this
     */
    public function addAction( $tag, $function_to_add, $priority = 10, $accepted_args = 1 )
    {

        add_action( $tag, [ 'Simplicity\Theme\Actions', $function_to_add ], $priority, $accepted_args );

        return $this;

    }

    /**
     * @param $function
     */
    public function widgetsInit( $function )
    {

        add_action( 'widgets_init', $function );

    }

    /**
     * Load Localisation files.
     *
     * @param string $domain Theme text domain.
     * @param array $paths   Paths to .po and .mo translation files.
     * @return $this
     */
    public function loadThemeTextdomain( $domain = '', $paths = [] )
    {

        $this->afterThemeSetup( function() use ( $domain, $paths ) {
            foreach ( $paths as $path ) {

                load_theme_textdomain( $domain, $path );

            }
        } );

        return $this;

    }

    /**
     * Sets up themes features.
     *
     * @param array $features Array of features.
     * @return $this
     */
    public function addThemeSupport( $features = [] )
    {

        $this->afterThemeSetup( function() use ( $features ) {
            foreach ( $features as $feature_k => $feature_v ) {

                if ( is_array( $feature_v ) ) {

                    add_theme_support( $feature_k, $feature_v );

                } else {

                    add_theme_support( $feature_v );

                }

            }
        } );

        return $this;

    }

    /**
     * Enqueue scripts.
     *
     * @param array $scripts Array list of scripts.
     * @return $this
     */
    public function enqueueScripts( $scripts = [] )
    {

        $this->enqueueAssets( function() use ( $scripts ) {
            foreach ( $scripts as $script_k => $script_v ) {

                if ( is_array( $script_v ) ) {

                    wp_enqueue_script( $script_k, isset( $script_v[0] ) ? $script_v[0] : '', isset( $script_v[1] ) ? $script_v[1] : [], isset( $script_v[2] ) ? $script_v[2] : false, isset( $script_v[3] ) ? $script_v[3] : false );

                } else {

                    wp_enqueue_script( $script_k );

                }

            }
        } );

        return $this;

    }

    /**
     * Enqueue styles.
     *
     * @param array $styles Array list of styles.
     * @return $this
     */
    public function enqueueStyles( $styles = [] )
    {

        $this->enqueueAssets( function() use ( $styles ) {
            foreach ( $styles as $style_k => $style_v ) {

                wp_enqueue_style( $style_k, isset( $style_v[0] ) ? $style_v[0] : '', isset( $style_v[1] ) ? $style_v[1] : [], isset( $style_v[2] ) ? $style_v[2] : false, isset( $style_v[3] ) ? $style_v[3] : 'all' );

            }
        } );

        return $this;

    }

    /**
     * Register our nav menus.
     *
     * @param array $menus
     * @return $this
     */
    public function registerNavMenu( $menus = [] )
    {

        $this->afterThemeSetup( function () use ( $menus ) {
            register_nav_menus( $menus );
        } );

        return $this;

    }

    /**
     * Register new image sizes.
     *
     * @param array $sizes
     * @return $this
     */
    public function addImageSize( $sizes = [] )
    {

        $this->afterThemeSetup( function () use ( $sizes ) {
            foreach ( $sizes as $size_k => $size_v ) {

                add_image_size( $size_k, isset( $size_v[0] ) ? $size_v[0] : '', isset( $size_v[1] ) ? $size_v[1] : '', isset( $size_v[0] ) ? $size_v[0] : false );

            }
        } );

        return $this;

    }

    /**
     * Register a sidebar.
     *
     * @param  array  $sidebars
     *
     * @return $this
     */
    public function registerSidebar( $sidebars = [] )
    {

        $this->widgetsInit( function () use ( $sidebars ) {
            foreach ( $sidebars as $sidebar ) {

                register_sidebar( $sidebar );

            }
        } );

        return $this;

    }

    /**
     * Register a widget.
     *
     * @param  array  $widgets
     *
     * @return $this
     */
    public function registerWidget( $widgets = [] )
    {

        $this->widgetsInit( function () use ( $widgets ) {
            foreach ( $widgets as $widget ) {

                register_widget( $widget );

            }
        } );

        return $this;

    }

    public function pagination() {
 
        if( is_singular() )
            return;
     
        global $wp_query;
     
        /** Stop execution if there's only 1 page */
        if( $wp_query->max_num_pages <= 1 )
            return;
     
        $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
        $max   = intval( $wp_query->max_num_pages );
     
        /** Add current page to the array */
        if ( $paged >= 1 )
            $links[] = $paged;
     
        /** Add the pages around the current page to the array */
        if ( $paged >= 3 ) {
            $links[] = $paged - 1;
            // $links[] = $paged - 2;
        }
     
        if ( ( $paged + 2 ) <= $max ) {
            // $links[] = $paged + 2;
            $links[] = $paged + 1;
        }
     
        echo '<div class="pagination"><ul class="clearfix">' . "\n";
     
        /** Previous Post Link */
        if ( get_previous_posts_link() )
            printf( '<li class="prev">%s</li>' . "\n", get_previous_posts_link('<') );
     
        /** Link to first page, plus ellipses if necessary */
        if ( ! in_array( 1, $links ) ) {
            $class = 1 == $paged ? ' class="active"' : '';
     
            printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );
     
            if ( ! in_array( 2, $links ) )
                echo '<li class="disabled">…</li>';
        }
     
        /** Link to current page, plus 2 pages in either direction if necessary */
        sort( $links );
        foreach ( (array) $links as $link ) {
            $class = $paged == $link ? ' class="active"' : '';
            printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
        }
     
        /** Link to last page, plus ellipses if necessary */
        if ( ! in_array( $max, $links ) ) {
            if ( ! in_array( $max - 1, $links ) )
                echo '<li class="disabled">…</li>' . "\n";
     
            $class = $paged == $max ? ' class="active"' : '';
            printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
        }
     
        /** Next Post Link */
        if ( get_next_posts_link() )
            printf( '<li class="next">%s</li>' . "\n", get_next_posts_link('>') );
     
        echo '</ul></div>' . "\n";
     
    }

    public function reading_time() {
        // load the content
        $thecontent = $post->post_content;
        // count the number of words
        $words = str_word_count( strip_tags( $thecontent ) );
        // rounding off and deviding per 200 words per minute
        $m = floor( $words / 200 );
        // rounding off to get the seconds
        $s = floor( $words % 200 / ( 200 / 60 ) );
        // calculate the amount of time needed to read
        $estimate = $m . ' minute' . ( $m == 1 ? '' : 's' ) . ', ' . $s . ' second' . ( $s == 1 ? '' : 's' );
        // create output
        $output = '<p>Estimated reading time: ' . $estimate . '</p>';
        // return the estimate
        return $output;
    }

}