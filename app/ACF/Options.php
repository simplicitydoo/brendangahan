<?php

namespace Simplicity\Theme\ACF;

class Options
{

    /**
     * Create options page.
     */
    public static function themeOptions()
    {

        return [
            'page_title'      => esc_html__( 'Theme Options', 'simplicity' ),
            'menu_slug'       => 'simplicity_theme_options',
            'capability'      => 'edit_plugins',
            'parent_slug'     => 'themes.php',
            'icon_url'        => 'dashicons-menu',
            'updated_message' => esc_html__( 'Theme Options have been successfully updated.', 'simplicity' )
        ];

    }

}