<?php
/**
 * Register Gutenberg Blocks via ACF.
 *
 * Read more here: https://www.advancedcustomfields.com/resources/acf_register_block_type/
 */

namespace Simplicity\Theme\ACF;

class Blocks
{

    public static function social_media_block()
    {

        return [
            'name'              => 'my-social',
            'title'             => esc_html__( 'My Social Media', 'simplicity' ),
            'description'       => esc_html__( 'Block for displaying social media links.', 'simplicity' ),
            'render_template'   => 'template-parts/blocks/social.php',
            'category'          => 'social',
            'icon'              => 'format-chat',
            'keywords'          => [ 'social', 'social media', 'facebook', 'twitter' ]
        ];

    }

    public static function newsletter_block()
    {

        return [
            'name'              => 'my-newsletter',
            'title'             => esc_html__( 'My Newsletter', 'simplicity' ),
            'description'       => esc_html__( 'Block for displaying newsletter.', 'simplicity' ),
            'render_template'   => 'template-parts/blocks/newsletter.php',
            'category'          => 'newsletter',
            'icon'              => 'format-chat',
            'keywords'          => [ 'newsletter', 'subscribe' ]
        ];

    }

}