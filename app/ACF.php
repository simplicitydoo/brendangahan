<?php
/**
 * Define our ACF Class.
 */

namespace Simplicity\Theme;

use Simplicity\Theme\ACF\Blocks;
use Simplicity\Theme\ACF\Options;

class ACF
{

    /**
     * Hook a function to "acf/init" action.
     *
     * @param $function Callback function.
     */
    public function acfInit( $function )
    {

        add_action( 'acf/init', function () use ( $function ) {
            call_user_func( $function );
        } );

    }

    /**
     * Create options page(s).
     *
     * @param array $options
     * @return $this
     */
    public function addOptionsPage( $options = [] )
    {

        $this->acfInit( function () use ( $options ) {
            foreach ( $options as $option ) {

                acf_add_options_page( call_user_func( [ Options::class, $option ] ) );

            }
        } );

        return $this;

    }

    /**
     * Create Gutenberg blocks.
     *
     * @param array $blocks
     * @return $this
     */
    public function addBlocks( $blocks = [] )
    {

        $this->acfInit( function () use ( $blocks ) {
            foreach ( $blocks as $block ) {

                $blockData = call_user_func( [ Blocks::class, $block ] );
                $blockData['enqueue_assets'] = function() {

                    if ( is_admin() ) {

                        wp_enqueue_style( 'simplicity-admin', get_theme_file_uri( 'assets/css/style-admin.css' ) );

                    }

                };

                acf_register_block_type( $blockData );

            }
        } );

        return $this;

    }

}