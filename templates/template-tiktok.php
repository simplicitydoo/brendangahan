<?php 
/**
 * Template Name: Tiktok Page
 */
get_header(); ?>
<?php
    $newsletter_heading = get_field('page_newsletter_heading');
    $newsletter_subheading = get_field('page_newsletter_subheading');
?>
<div class="container container--tiktok">
    <div class="lg:w-8/12 w-full mx-auto pt-10 lg:pt-18">
        <?php

            if ( have_posts() ) {

                while ( have_posts() ) {
                    the_post();

                    get_template_part( 'template-parts/content', 'page' );
                    
                }
            }

        ?>
    </div>
    <div class="lg:w-10/12 w-full mx-auto">
        <div id="newsletter-tiktok" class="newsletter newsletter--bottom pb-14 pt-8 text-left mb-10">
            <div class="">
                <div class="lg:w-4/5 w-full mx-auto">
                    <?php get_template_part('template-parts/content', 'tiktok-form'); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
get_footer();