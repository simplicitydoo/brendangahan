<?php 
/**
 * Template Name: Search Page
 */
get_header(); ?>
<div class="container container--search">
    <div class="lg:w-8/12 w-full mx-auto">
        <h1 class="text-h1 leading-h1 font-bold text-center my-8"><?php the_title(); ?></h1>
        <?php get_search_form(); ?>
        <div class="site-tags text-center">
            <h4 class="text-h4 leading-h4 font-bold mb-6"><?php _e('Popular Tags', 'simplicity'); ?></h4>
            <?php
                $tags = get_tags(array(
                    'orderby' => 'count',
                    'order' => 'DESC',
                    'number'  => 15,
                 ));
                $html = '<div class="post_tags mb-12 clearfix">';
                foreach ( $tags as $tag ) {
                    $tag_link = get_tag_link( $tag->term_id );
                             
                    $html .= "<a href='{$tag_link}' title='{$tag->name} Tag' class='tag__single inline-block bg-other-gray-8 py-1 px-6 bg-other text-category leading-category text-other-gray-75 mr-1 mb-1'> ";
                    $html .= "{$tag->name}</a>";
                }
                $html .= '</div>';
                echo $html;
            ?>
        </div>
    </div>
</div>
<?php
get_footer();