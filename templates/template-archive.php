<?php
/**
 * Template Name: Yearly archive
 */
get_header();
?>
<div class="container container--search">
    <div class="lg:w-8/12 w-full mx-auto">
        <?php

            if ( have_posts() ) {

                while ( have_posts() ) {
                    the_post();

                    // Repeater for external links
                    $around_the_net = get_field('posts_for_other_websites');

                    get_template_part( 'template-parts/content', 'page' );
                    
                    
                }
            }

        ?>
        <div class="yearly-archive--content">
        <?php 

            $years = $wpdb->get_results( "SELECT YEAR(post_date) AS year FROM wp_posts WHERE post_type = 'post' AND post_status = 'publish' GROUP BY year DESC" );

            foreach ( $years as $year ) {
                
                // get posts for each year
                $posts_this_year = $wpdb->get_results( "SELECT * FROM wp_posts WHERE post_type = 'post' AND post_status = 'publish' AND YEAR(post_date) = '" . $year->year . "'" );
                $posts_this_year_reversed = array_reverse($posts_this_year);
                echo '<div class="row--year lg:flex lg:space-x-6" data-aos="fade-up" data-aos-duration="1000"><div class="bt-4 w-full lg:w-1/4"><div class="archive-year">' . $year->year . '</div></div><div class="bt-4 w-full lg:w-3/4">';

                foreach ( $posts_this_year_reversed as $post ) {
                    echo '<div class="bb-2 archive-post--single"><div class="archive-post-title"><h5 class="post-title"><a href="' . get_permalink( $post->ID ) . '">' . $post->post_title . '</a></h5></div>';   
                    echo '<div class="post-reading-time">' . reading_time() . ' read</div></div>'; 
                }
                echo '</div></div>';
            } 
        ?>
        </div>
        <div class="yearly-archive--content yearly-archive--content--v2" data-aos="fade-up" data-aos-duration="1000">
        <?php 

            if ( $around_the_net ){
                echo '<div class="row--year block lg:flex lg:space-x-6"><div class="bt-4--red w-full lg:w-1/4"><div class="archive-year">Around<br>The Net</div></div><div class="bt-4--red w-full lg:w-3/4">';
                foreach ( $around_the_net as $row ) {
                    // get posts for each year
            
                    echo '<div class="bb-2 archive-post--single"><div class="archive-post-title"><h5 class="post-title"><a href="' . $row['url'] . '" target="_blank">' . $row['title'] . ' <svg class="inline" width="18" height="16" xmlns="http://www.w3.org/2000/svg"><path d="M7.994 11.774l7.61-7.61L16.72 5.28c.469.469 1.28.14 1.28-.53v-4a.75.75 0 00-.75-.75h-4c-.667 0-1 .81-.53 1.28l1.116 1.116-7.61 7.61a.75.75 0 000 1.06l.707.708a.75.75 0 001.06 0zM12.5 16a1.5 1.5 0 001.5-1.5V8.993a.75.75 0 00-1.28-.53l-.5.5a.875.875 0 00-.209.42l-.011.11V14H2V4h8.25c.171 0 .41-.098.53-.22l.5-.5A.75.75 0 0010.75 2H1.5A1.5 1.5 0 000 3.5v11A1.5 1.5 0 001.5 16h11z" fill="#191919" fill-rule="nonzero"/></svg></a></h5></div>';   
                    echo '<div class="post-reading-time">Published on <span class="font-bold">' . $row['published_on'] . '</span></div></div>'; 

                    
                } 
                echo '</div></div>';
            }
        ?>
        </div>
    </div>
</div>

<?php
get_footer();