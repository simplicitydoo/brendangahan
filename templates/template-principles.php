<?php
/**
 * Template Name: Principles Of Social
 */
get_header();
?>
<div class="container container--search">
    <div class="lg:w-8/12 w-full mx-auto">
        <?php

            if ( have_posts() ) {

                while ( have_posts() ) {
                    the_post();
                    $list_of_articles = get_field('list_of_my_favorite_articles');
                    $count = count($list_of_articles);

                    get_template_part( 'template-parts/content', 'page' );
                    
                }
            }

        ?>
        <?php if( $list_of_articles ): ?>
        <div class="lists" data-aos="fade-up" data-aos-duration="1000">
        <?php
            $counter = 1;
            foreach ( $list_of_articles as $article ){
                echo '<div class="single-list text-normal font-bold leading-normal text-other-red underline mb-6"><a href="#list-' . $counter . '" class="block pl-6 relative"><svg class="inline absolute left-0 top-2" width="16" height="16" xmlns="http://www.w3.org/2000/svg"><path d="M8.251 11.75l-1.843 1.843c-.95.949-2.489.949-3.438 0l-.588-.588a2.432 2.432 0 010-3.438l2.451-2.452a2.442 2.442 0 013.438 0l.877.877a.847.847 0 001.196-1.196l-.877-.877a4.126 4.126 0 00-5.8 0L1.196 8.371A4.066 4.066 0 000 11.28a3.986 3.986 0 001.196 2.89l.588.588a4.096 4.096 0 005.78 0l1.843-1.844a.837.837 0 000-1.196.807.807 0 00-1.156.03zm5.93-10.554a4.096 4.096 0 00-5.78 0L6.846 2.75a.847.847 0 101.196 1.196l1.555-1.554c.95-.95 2.488-.95 3.438 0l.588.588c.949.95.949 2.488 0 3.438L11.46 8.55c-.95.95-2.488.95-3.438 0l-.877-.877A.856.856 0 105.93 8.88l.877.877a4.096 4.096 0 005.78 0l2.133-2.132a4.096 4.096 0 000-5.78l-.538-.648z" fill="#BC3428" fill-rule="nonzero"/></svg> ' . $article['list_title'] . '</a></div>';
                $counter++;
            }
        ?>
        </div>
        <div class="lists__content" data-aos="fade-up" data-aos-duration="1400">
        <?php
            $counter = 1;
            foreach ( $list_of_articles as $article ){
                echo '<div id="list-' . $counter . '" class="list-header p-4 bg-other-gray-8 text-other-dark font-bold text-h4 leading-h4">' . $article['list_title'] . '</div>';
                echo '<div class="articles-list px-4">';
                    foreach ( $article['articles'] as $post ){
                        echo '<div class="articles-list__single py-6"><div class="mb-4 text-h5 leading-h5 font-bold"><a href="' . get_the_permalink($post) . '">' . get_the_title($post) . '</a></div>';

                        if ( has_excerpt() ) {
                            echo '<div class="articles-list__excerpt font-body text-normal leading-normal">';
                                the_excerpt();
                            echo '</div>';
                        } else {
                            $content = get_the_excerpt();
                            $new_content = substr( $content, 0, strpos($content, '.') +1);
                            $new_content = strip_tags($new_content, '<a><strong><em>');
                            echo '<div class="articles-list__excerpt font-body text-normal leading-normal">' . $new_content . '</div>';
                        }
                        echo '</div>';
                    }
                    $counter++;
                echo '</div>';
            }
        ?>
        </div>
        <?php endif; ?>
    </div>
</div>

<?php
get_footer();