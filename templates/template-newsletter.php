<?php 
/**
 * Template Name: Newsletter Page
 */
get_header(); ?>
<?php
    $newsletter_heading = get_field('page_newsletter_heading');
    $newsletter_subheading = get_field('page_newsletter_subheading');
?>
<div class="container container--search">
    <div class="lg:w-8/12 w-full mx-auto">
        <?php

            if ( have_posts() ) {

                while ( have_posts() ) {
                    the_post();

                    get_template_part( 'template-parts/content', 'page' );
                    
                }
            }

        ?>
    </div>
    <div class="lg:w-10/12 w-full mx-auto">
        <div id="newsletter-bottom" class="newsletter newsletter--bottom pb-14 pt-8 bg-other-gray-8 text-left mb-10">
            <div class="container">
                <div class="lg:w-4/5 w-10/12 mx-auto">
                    <?php if ( $newsletter_heading ): ?>
                        <div class="text-h2 leading-h2 font-head font-bold mb-4 text-other-dark"><?php echo esc_html( $newsletter_heading ); ?></div>
                    <?php endif; ?>
                    <?php if ( $newsletter_subheading ): ?>
                        <div class="mb-6 text-normal font-body leading-normal text-other-dark"><?php echo esc_html( $newsletter_subheading ); ?></div>
                    <?php endif; ?>
                    <?php get_template_part('template-parts/content', 'newsletter-form'); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
get_footer();