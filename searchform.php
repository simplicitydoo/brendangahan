<div class="search-form-holder">
    <form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
        <label>
            <input type="search" class="search-field font-body text-other-dark leading-normal text-normal"
                placeholder="<?php echo esc_attr_x( 'Type to search and press Enter …', 'placeholder' ) ?>"
                value="<?php echo get_search_query() ?>" name="s"
                title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />
        </label>
    </form>
    <button class="clear-input"></button>
</div>