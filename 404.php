<?php get_header(); ?>
<?php
    $title = get_field('404_title', 'option');
    $message = get_field('404_message', 'option');
    $allowed_html = array(
        'a' => array(
        'href' => array(),
        'title' => array()
    ),
        'br' => array(),
        'em' => array(),
        'strong' => array(),
        'p' => array()
    );
?>
<div class="container container--search">
    <div class="lg:w-8/12 w-full mx-auto">
        <h1 class="text-h1 leading-h1 font-bold text-center my-8"><?php echo wp_kses( $title, [ 'br' => [] ] ); ?></h1>
        <div class="text-center mb-8">
            <img class="mx-auto" src="<?php echo get_template_directory_uri() . '/assets/images/404.svg'; ?>">
        </div>
        <div class="404-message text-center mb-8 post-content">
            <?php echo wp_kses( $message, $allowed_html ); ?>
        </div>
    </div>
</div>
<?php
get_footer();