<!DOCTYPE html>
<html <?php language_attributes(); ?> class="antialiased leading-normal">
<head>
    <!-- Meta -->
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=2.0">

    <!-- Other -->
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

    <?php wp_head(); ?>
</head>
<body <?php body_class( 'font-sans' ); ?> x-data="{}" x-cloak>
<?php 
    wp_body_open(); 
    $custom_logo_id = get_theme_mod( 'custom_logo' );
    $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
    $search_page = get_field('search_page', 'option');
?>
<header id="side-header" class="<?php echo ( is_front_page() ? 'absolute top-0 inset-x-0 ' : 'border-0 lg:border-b lg:bordr-b-other-gray-8 lg:border-b-2 '); ?>lg:py-4 z-10">
    <div class="container container--head flex flex-wrap relative px-0 lg:pr-10 lg:pl-0">
        <div class="logo relative w-full lg:w-12 block lg:inline-flex lg:py-0 lg:px-0 px-4 border-b bordr-b-other-gray-8 border-b-2 lg:border-0 lg:py-0 py-4">
            <?php 
                if ( has_custom_logo() ) {
                    echo '<a href="' . get_option("siteurl") . '"><img src="' . esc_url( $logo[0] ) . '" alt="' . get_bloginfo( 'name' ) . '"></a>';
                } else {
                        echo '<h1><a href="' . get_option("siteurl") . '">'. get_bloginfo( 'name' ) .'</a></h1>';
                }
            ?>
            <button id="menu-toggle" class="lg:hidden block"></button>
        </div>
        <?php echo ( is_front_page() ? '<div class="hidden">' : ''); ?>
        <div class="menu-wrapper hidden w-full lg:w-auto lg:flex text-center lg:text-right lg:ml-auto">
            <?php
                wp_nav_menu( [
                        'menu'            => 'primary',
                        'menu_class'      => 'block lg:flex uppercase font-head',
                        'container'       => 'nav',
                        'container_class' => 'block lg:flex menu menu--header justify-between items-center',
                        'theme_location'  => 'primary'
                ] );
            ?>
            <?php
                if ( $search_page ){
                    echo '<a href="' . $search_page . '"class="open-search-page w-4 h-4 relative lg:absolute right-0 m-auto inset-y-0 block"></a>';
                }
            ?>
        </div>
        <?php echo ( is_front_page() ? '</div>' : ''); ?>
    </div>
</header>