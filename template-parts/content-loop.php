<article id="content-<?php the_ID(); ?>" <?php post_class( 'relative' ); ?>>
    <div class="relative block">
        <div class="flex flex-wrap content-center">
            <div class="w-full">
                <h3 class="post-content__title font-bold"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                <?php if ( is_archive() || is_home() ): ?>
                    <div class="post-content__reading-time mb-6"><?php echo reading_time(); ?> read</div>
                    <?php if( has_post_thumbnail() ): ?>
                        <div class="post-content__featured-image mb-6">
                            <?php the_post_thumbnail( 'post-featured', [ 'class' => '' ] ); ?>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="post-content__excerpt w-full">
        <div class="post-content__excerpt-inner">
            <?php 
                if ( has_excerpt() ) {
                    the_excerpt();
                } else {
                    $content = get_the_excerpt();
                    $new_content = substr( $content, 0, strpos($content, '.') +1);
                    $new_content = strip_tags($new_content, '<a><strong><em>');
                    echo $new_content;
                }
            ?>
        </div>
    </div>
    <div class="block post-content__read-more post-content">
        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php _e('Read More...', 'simlicity'); ?></a>
    </div>
</article>