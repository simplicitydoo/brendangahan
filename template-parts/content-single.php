<article id="content-<?php the_ID(); ?>" <?php post_class( 'relative post-content' ); ?>>
    <div class="relative block">
        <div class="flex flex-wrap content-center">
            <div class="w-full text-center pt-8">
                <h1 class="font-head text-h1 font-bold leading-h1 mb-8"><?php the_title(); ?></h1>
                <div class="font-head text-category leading-category text-other-gray-75 font-medium mb-8">Reading time: <span class="text-other-dark"><?php echo reading_time(); ?></span></div>
                <?php if( has_post_thumbnail() ): ?>    
                    <div class="featured-image mb-12">
                        <?php the_post_thumbnail( 'post-featured', [ 'class' => '' ] ); ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="w-full">
        <div>
            <?php the_content(); ?>
        </div>
        <div class="categories text-other-gray-75 my-8">
            <div class="font-medium font-head text-category leading-category text-other-dark">
                <?php _e('Posted in:', 'simplicity' ); ?>
            </div>
            <div class="text-category leading-category">
                <?php
                    $categories = get_the_category();
                    $separator = ', ';
                    $output = '';
                    if ( ! empty( $categories ) ) {
                        foreach( $categories as $category ) {
                            $output .= '<a href="' . esc_url( get_category_link( $category->term_id ) ) . '" class="category__single text-category leading-category text-other-gray-75" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ) . '">' . esc_html( $category->name ) . '</a>' . $separator;
                        }
                        echo trim( $output, $separator );
                    }
                ?>
            </div>
        </div>
        <div class="tags text-other-gray-75 mb-8">
            <div class="font-medium font-head text-category leading-category text-other-dark">
                <?php _e('Tagged as:', 'simplicity' ); ?>
            </div>
            <div class="text-category leading-category">
                <?php
                    $tags = get_the_tags();
                    $separator = '';
                    $output = '';
                    if ( ! empty( $tags ) ) {
                        foreach( $tags as $tag ) {
                            $output .= '<a href="' . esc_url( get_category_link( $tag->term_id ) ) . '" class="tag__single py-1 px-6 bg-other text-category leading-category text-other-gray-75 mr-1 mb-1" alt="' . esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $tag->name ) ) . '">' . esc_html( $tag->name ) . '</a>' . $separator;
                        }
                        echo trim( $output, $separator );
                    }
                ?>
            </div>
        </div>
    </div>
</article>