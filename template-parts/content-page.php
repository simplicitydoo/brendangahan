<article id="content-<?php the_ID(); ?>" <?php post_class( 'relative post-content' ); ?>>
    <div class="relative block">
        <div class="flex flex-wrap content-center">
            <div class="w-full pt-8">
                <h1 class="font-head text-h1 font-bold leading-h1 mb-8"><?php the_title(); ?></h1>
                <?php if( has_post_thumbnail() ): ?>    
                    <div class="featured-image mb-12">
                        <?php the_post_thumbnail( 'post-featured', [ 'class' => '' ] ); ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="w-full">
        <div>
            <?php the_content(); ?>
        </div>
    </div>
</article>