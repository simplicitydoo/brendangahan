<?php
/**
 * Block Name: My Social Media
 *
 */

$title = get_field('nb_block_title');
$subtitle = get_field('nb_block_subtitle');

// create id attribute for specific styling
$id = 'newsletter-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

?>
<div id="<?php echo $id; ?>" class="bg-other-dark px-4 pt-6 pb-8 my-12 newsletter-block <?php echo $align_class; ?>">
    <div id="shortcode-newsletter" class="text-center">
        <?php if ( $title ): ?>
            <h4 class="mb-4 text-white"><?php echo esc_html( $title ); ?></h4>
        <?php endif; ?>
        <?php if ( $subtitle ): ?>
            <p class="mb-4 text-white"><?php echo esc_html( $subtitle ); ?></p>
        <?php endif; ?>
        <div class="newsletter newsletter--block">
            <?php get_template_part('template-parts/content', 'newsletter-form'); ?>
        </div>
    </div>
</div>
<style type="text/css">
	#<?php echo $id; ?> {
		background: <?php the_field('background_color'); ?>;
		color: <?php the_field('text_color'); ?>;
    }
    
</style>