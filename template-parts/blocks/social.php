<?php
/**
 * Block Name: My Social Media
 *
 */

$title = get_field('smb_block_title');
$subtitle = get_field('smb_block_subtitle');
$tiktok = get_field('tiktok', 'option');
$linkedin = get_field('linkedin', 'option');
$twitter = get_field('twitter', 'option');

// create id attribute for specific styling
$id = 'social-media-' . $block['id'];

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

?>
<div id="<?php echo $id; ?>" class="social-media-block <?php echo $align_class; ?>">
    <?php if ( $title ): ?>
        <h4 class="font-head"><?php echo esc_html( $title ); ?></h4>
    <?php endif; ?>
    <?php if ( $subtitle ): ?>
        <p class="font-body"><?php echo esc_html( $subtitle ); ?></p>
    <?php endif; ?>
    <ul class="social-media social-media--block">
        <?php if ( $twitter ): ?>
            <li class="inline-block"><a href="<?php echo $twitter; ?>" title="Twitter" target="_blank"><img width="32" height="26" src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/social/twitter.svg"></a></li>
        <?php endif; ?>
        <?php if ( $linkedin ): ?>
            <li class="inline-block"><a href="<?php echo $linkedin; ?>" title="Linkedin" target="_blank"><img width="27" height="27" src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/social/linkedin-in.svg"></a></li>
        <?php endif; ?>
        <?php if ( $tiktok ): ?>
            <li class="inline-block"><a href="<?php echo $tiktok; ?>" title="Tiktok" target="_blank"><img width="25" height="25" src="<?php echo get_template_directory_uri(); ?>/assets/images/icons/social/tiktok.svg"></a></li>
        <?php endif; ?>
    </ul>
</div>
<style type="text/css">
    #<?php echo $id . ' ul'; ?> {
        padding: 0;
        margin: 0;
        color: #fff;
    }
</style>