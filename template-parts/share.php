<div class="share">
    <div class="">
        <div class="w-full mx-auto">
            <div class="share__inner block lg:flex content-center">
                <div class="w-full lg:w-3/5 text-center lg:text-left mb-8 lg:mb-0">
                    <span class="font-head font-bold text-h5 leading-h4"><?php _e( 'If you liked this article please consider sharing it.', 'simplicity' ); ?></span>
                </div>
                <div class="w-full lg:w-2/5 flex items-center">
                    <ul class="social-media mx-auto text-center">
                        <li class="inline-block">
                            <?php
                                echo '<a target="_new" href="http://www.facebook.com/share.php?u=' . urlencode(get_the_permalink()) . '&title=' . urlencode(get_the_title()). '"><img width="16" height="30" src="' . get_template_directory_uri() .'/assets/images/icons/social/facebook-f.svg"></a>';
                            ?>
                        </li>
                        <li class="inline-block">
                            <?php
                                echo '<a target="_new" href="http://www.linkedin.com/shareArticle?mini=true&url=' . urlencode(get_the_permalink()) . '&title=' . urlencode(get_the_title()) . '&source=' . get_bloginfo("url") . '"><img width="27" height="27" src="' . get_template_directory_uri() .'/assets/images/icons/social/linkedin-in.svg"></a>';
                            ?>
                        </li>
                        <li class="inline-block">
                            <?php
                                echo '<a target="_new" href="http://twitter.com/intent/tweet?text=' . urlencode(get_the_title()) . '+' . urlencode(get_the_permalink()) . '"><img width="32" height="26" src="' . get_template_directory_uri() . '/assets/images/icons/social/twitter.svg"></a>';
                            ?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>