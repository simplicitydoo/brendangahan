<?php
    //for use in the loop, list 3 post titles related to first category on current post
    $cats = get_the_terms( $post->ID, 'category' );

    $first_cat = $cats[0]->term_id;

    $related_args = [
        'category__in'          => array( $first_cat ),
        'post__not_in'          => array( $post->ID ),
        'posts_per_page'        => 3,
        'ignore_sticky_posts'   => 1,
        'post_status' => 'publish'
    ];
    $related_query = new WP_Query( $related_args );

    if( $related_query->have_posts() ) { ?>
        <div class="related-posts">
            <div class="text-center related-posts__title uppercase font-bold">
                <?php _e('Articles You May Like', 'simplicity'); ?>
            </div>
            <div class="block lg:flex lg:space-x-6">
                <?php while ($related_query->have_posts()) : $related_query->the_post(); ?>

                        <div class="w-full lg:w-4/12">

                            <?php get_template_part( 'template-parts/content', 'loop' ); ?>

                        </div>

                    <?php

                endwhile; ?>
            </div>
        </div>
    <?php }
    wp_reset_query();
?>
