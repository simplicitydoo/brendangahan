
<style>
    .formkit-form[data-uid="6583421629"] {border: 0 !important;}
    .formkit-header {display: none !important;}
</style>
<script async data-uid="6583421629" src="https://brendan-gahan.ck.page/6583421629/index.js"></script>
<?php if ( !is_front_page() ) : ?>
        <style>
            .formkit-form {
                background-color: transparent !important;
                font-family: "source_serif_pro", Georgia, Cambria, "Times New Roman", Times, serif !important;
            }
            .formkit-guarantee p,
            .formkit-subheader p {
                text-align: center !important;
            }
            .formkit-form[data-uid="6583421629"][min-width~="600"] [data-style="minimal"] {
                padding: 0;
            }
            .formkit-form[data-uid="6583421629"] .formkit-input {
                padding: 16px 12px !important; 
            }
        </style>
<?php endif; ?>