<?php get_header(); ?>
<div class="container container--search">
    <div class="lg:w-8/12 w-full mx-auto">
        <?php

            if ( have_posts() ) {

                while ( have_posts() ) {
                    the_post();

                    get_template_part( 'template-parts/content', 'single' );
                    get_template_part( 'template-parts/share' );
                    
                }
            }

        ?>
    </div>
    <!-- <?php get_template_part( 'template-parts/share' ); ?> -->
    <div class="w-full">
        <?php get_template_part( 'template-parts/content', 'related-posts' ); ?>
    </div>
</div>
<?php
get_footer();