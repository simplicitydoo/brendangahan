// Require Laravel Mix
let mix = require('laravel-mix');
// Require Tailwind CSS
let tailwindcss = require('tailwindcss');
// Do Laravel Mix
mix.setPublicPath( '/' )
    .sass( 'assets/scss/app.scss', 'style.css' )
    .sass( 'assets/scss/app-admin.scss', 'assets/css/style-admin.css' )
    .copy( 'node_modules/alpinejs/dist/alpine.js', 'assets/js' )
    .minify( 'assets/js/alpine.js', 'assets/js' )
    .minify( 'assets/js/app.js', 'assets/js' )
    .options({
        processCssUrls: false,
        postCss: [ tailwindcss('tailwind.config.js') ]
    });