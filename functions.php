<?php
/**
 * Theme Engine Room.
 */

/**
 * Require Autoloader
 */
require_once get_theme_file_path( 'vendor/autoload.php' );

/**
 * Import classes.
 */
use Simplicity\Theme\{ Main, ACF, Helpers };

/**
 * Assign theme classes to an object.
 */
$Main = new Main();
$ACF  = new ACF();

/**
 * Set theme defaults.
 */
$scripts = [
    'simplicity-alpinejs' => [
        get_theme_file_uri( 'assets/js/alpine' . Helpers::getMinifiedPrefix() . '.js' ),
        [],
        '2.6.0',
        false
    ],
    'aos' => [
        get_theme_file_uri( '/assets/js/aos.js' ),
        [],
        '1.0.0',
        true
    ],
    'simplicity' => [
        get_theme_file_uri( 'assets/js/app' . Helpers::getMinifiedPrefix() . '.js' ),
        [ 'jquery' ],
        '1.0.0',
        true
    ]
];
if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {

    array_push( $scripts, 'comment-reply' );

}
$Main->loadThemeTextdomain(
    'simplicity',
    [
        trailingslashit( WP_LANG_DIR ) . 'themes',
        get_stylesheet_directory() . '/languages',
        get_template_directory() . '/languages'
    ]
)->addThemeSupport(
    [
        'automatic-feed-links',
        'post-thumbnails',
        'html5' => [
            'comment-list',
            'comment-form',
            'search-form',
            'gallery',
            'caption',
            'widgets'
        ],
        'title-tag',
        'customize-selective-refresh-widgets',
        'wp-block-styles',
        'align-wide',
        'editor-styles',
        'responsive-embeds',
        'custom-logo'
    ]
)->registerNavMenu(
    [
        'primary' => esc_html__( 'Primary Menu', 'simplicity' ),
        'footer' => esc_html__( 'Footer Menu', 'simplicity' ),
        'footer_home' => esc_html__( 'Footer Home Menu', 'simplicity' )
    ]
)->enqueueStyles(
    [
        'simplicity' => [
            get_theme_file_uri( 'style.css' ),
            [],
            '1.0.99'
        ],
        'aos' => [
            get_theme_file_uri( '/assets/css/aos.css' ),
            [],
            '1.0.0'
        ]
    ]
)->addImageSize(
    [
        'post-featured' => [
            '666',
            '370'
        ],
        'post-full-width-of-container' => [
            '1000',
            '380'
        ]
    ]
)->enqueueScripts(
    $scripts
);

/**
 * Set ACF Defaults.
 */
$ACF->addOptionsPage(
    [
        'themeOptions'
    ]
)->addBlocks(
    [
        'social_media_block',
        'newsletter_block'
    ]
);

/* Reading time function */

function reading_time() {
    global $post;
    // load the content
    $thecontent = $post->post_content;
    // count the number of words
    $words = str_word_count( strip_tags( $thecontent ) );
    // rounding off and deviding per 200 words per minute
    $m = floor( $words / 200 );
    // rounding off to get the seconds
    $s = floor( $words % 200 / ( 200 / 60 ) );
    if ( $s > 30 ){
        $m++;
    } else if ( $m == 0 ){
        $m = 1;
    }
    // calculate the amount of time needed to read
    $estimate = $m . ' min';
    // create output
    $output = $estimate;
    // return the estimate
    return $output;
}

function pagination() {
 
    if( is_singular() )
        return;
 
    global $wp_query;
 
    /** Stop execution if there's only 1 page */
    if( $wp_query->max_num_pages <= 1 )
        return;
 
    $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
    $max   = intval( $wp_query->max_num_pages );
 
    /** Add current page to the array */
    if ( $paged >= 1 )
        $links[] = $paged;
 
    /** Add the pages around the current page to the array */
    if ( $paged >= 3 ) {
        $links[] = $paged - 1;
        // $links[] = $paged - 2;
    }
 
    if ( ( $paged + 2 ) <= $max ) {
        // $links[] = $paged + 2;
        $links[] = $paged + 1;
    }
 
    echo '<div class="pagination"><ul class="clearfix">' . "\n";
 
    /** Previous Post Link */
    if ( get_previous_posts_link() )
        printf( '<li class="prev">%s</li>' . "\n", get_previous_posts_link('<') );
 
    /** Link to first page, plus ellipses if necessary */
    if ( ! in_array( 1, $links ) ) {
        $class = 1 == $paged ? ' class="active"' : '';
 
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );
 
        if ( ! in_array( 2, $links ) )
            echo '<li class="disabled">…</li>';
    }
 
    /** Link to current page, plus 2 pages in either direction if necessary */
    sort( $links );
    foreach ( (array) $links as $link ) {
        $class = $paged == $link ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
    }
 
    /** Link to last page, plus ellipses if necessary */
    if ( ! in_array( $max, $links ) ) {
        if ( ! in_array( $max - 1, $links ) )
            echo '<li class="disabled">…</li>' . "\n";
 
        $class = $paged == $max ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
    }
 
    /** Next Post Link */
    if ( get_next_posts_link() )
        printf( '<li class="next">%s</li>' . "\n", get_next_posts_link('>') );
 
    echo '</ul></div>' . "\n";
 
}
